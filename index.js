'use strict';
const excelToJson = require('convert-excel-to-json');
 
const result = excelToJson({
    sourceFile: 'SOME-EXCEL-FILE.xlsx',
    sheets:[{
        name: 'sheet1',
        columnToKey: {
        	A: 'id',
    		B: 'ProductName'
        }
    },{
        name: 'sheet2',
        columnToKey: {
        	A: 'id',
    		B: 'ProductDescription'
        }
    }]
});